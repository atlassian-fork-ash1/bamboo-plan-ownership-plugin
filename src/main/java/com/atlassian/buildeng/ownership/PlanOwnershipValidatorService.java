/*
 * Copyright 2018 - 2019 Atlassian Pty Ltd.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.atlassian.buildeng.ownership;

import com.atlassian.bamboo.plan.Plan;
import com.atlassian.bamboo.plan.PlanKey;
import com.atlassian.bamboo.plan.PlanKeys;
import com.atlassian.bamboo.plan.PlanManager;
import com.atlassian.bamboo.plan.PlanPermissionsService;
import com.atlassian.bamboo.plan.cache.ImmutablePlan;
import com.atlassian.bamboo.security.BambooPermissionManager;
import com.atlassian.bamboo.security.acegi.acls.BambooPermission;
import com.atlassian.bamboo.user.BambooUser;
import com.atlassian.bamboo.user.BambooUserManager;
import com.atlassian.buildeng.ownership.plan.PlanOwnershipPlanConfigurationPlugin;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;

import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.acegisecurity.AccessDeniedException;
import org.jetbrains.annotations.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@ExportAsService
@Component
public class PlanOwnershipValidatorService {
    private static final Logger log = LoggerFactory.getLogger(PlanOwnershipValidatorService.class);

    private final BambooUserManager bambooUserManager;
    private final PlanManager planManager;
    private final BambooPermissionManager bambooPermissionManager;


    public static final String NO_OWNER = "No user has been set for this plan.";
    public static final String DOESNT_EXIST = "This user does not exist. Please choose a valid username.";
    public static final String INACTIVE_USER = "This user is inactive. Please choose an active user.";
    public static final String BOT_ACCOUNT = "Bot accounts are not allowed. Please choose a human user.";
    
    /**
     * this is purely here to support yaml bamboo specs that don't allow custom plugin config to propagate.
     */
    private static final Pattern planNamePattern = Pattern.compile(".* Owner\\=([a-zA-Z0-9]+)$");

    @Autowired
    public PlanOwnershipValidatorService(BambooUserManager bambooUserManager,
                                         PlanManager planManager,
                                         BambooPermissionManager bambooPermissionManager) {
        this.bambooUserManager = bambooUserManager;
        this.planManager = planManager;
        this.bambooPermissionManager = bambooPermissionManager;
    }

    public static boolean isBotAccount(String owner) {
        Pattern p = Pattern.compile(".*-bot.*");
        Matcher m = p.matcher(owner);
        return m.matches();
    }

    public String getReasonForSuggestedOwner(String suggestedOwner) {
        return getReason(suggestedOwner);
    }

    public String getReasonForExistingOwner(String planKey) {
        String ownerFromConfig = getOwnerFromConfig(planKey);
        return getReason(ownerFromConfig);
    }

    private String getReason(String owner) {
        String reason = "";
        BambooUser bambooUser = bambooUserManager.getBambooUser(owner);

        if (owner == null) {
            reason = NO_OWNER;
        } else if (bambooUser == null) {
            reason = DOESNT_EXIST;
        } else if (!bambooUser.isEnabled()) {
            reason = INACTIVE_USER;
        } else if (isBotAccount(bambooUser.getUsername())) {
            reason = BOT_ACCOUNT;
        }
        return reason;
    }

    public boolean isExistingOwnerValid(String planKey) {
        String ownerFromConfig = getOwnerFromConfig(planKey);
        return isValid(ownerFromConfig);
    }

    public String getOwnerFromConfig(String planKey) {
        ImmutablePlan plan = getPlanByKey(planKey);
        return getOwnerFromConfig(plan);
    }

    public String getOwnerFromConfig(ImmutablePlan plan) {
        if (plan == null) {
            return null;
        }
        if (plan.hasMaster()) {
            plan = plan.getMaster();
        }
        Map<String, String> customConfig = getCustomConfiguration(plan);
        String ownerFromConfig = customConfig.get(PlanOwnershipPlanConfigurationPlugin.OWNER_OF_PLAN);
        if (ownerFromConfig == null) {
            Matcher matcher = planNamePattern.matcher(plan.getName());
            log.debug("plan:" + plan.getKey() + " matched:" + matcher.matches() + " for:" + plan.getName());
            if (matcher.matches()) {
                ownerFromConfig = matcher.group(1);
            }
        }
        return ownerFromConfig;
    }

    public boolean isSuggestedOwnerValid(String suggestedOwner) {
        return isValid(suggestedOwner);
    }

    private boolean isValid(String owner) {
        BambooUser bambooUser = bambooUserManager.getBambooUser(owner);
        return !(owner == null || bambooUser == null 
                || !bambooUser.isEnabled() || isBotAccount(bambooUser.getUsername()));
    }

    private Plan getPlanByKey(String planKey) {
        return  planManager.getPlanByKey(PlanKeys.getPlanKey(planKey));
    }
    
    @NotNull
    private Map<String, String> getCustomConfiguration(ImmutablePlan plan) {
        return plan.getBuildDefinition().getCustomConfiguration();
    }

    /**
     * Check if the logged-in user can edit the plan.
     */
    public Boolean userCanEditPlan(ImmutablePlan plan) {
        PlanKey planKey = plan.getPlanKey();

        if (planKey == null) {
            return false;
        }

        return bambooPermissionManager.hasPlanPermission(BambooPermission.ADMINISTRATION, planKey)
                || bambooPermissionManager.hasPlanPermission(BambooPermission.WRITE, planKey);
    }
}
